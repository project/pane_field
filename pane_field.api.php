<?php

/**
 * Hook to alter pane contexts.
 *
 * @param array $context
 *  The contexts applied to the pane
 *
 * @param array $data
 *  The data of the entity on which the pane is rendered.
 */
function hook_pane_field_context_alter(&$context, $data) {
  if ($data['entity_type'] == 'paragraphs_item') {
    $context_parent = ctools_context_create($data['entity']->hostEntityType(), $data['entity']->hostEntity());
    $context['context_' . $data['entity']->hostEntityType() . '_1'] = $context_parent;
  }
}